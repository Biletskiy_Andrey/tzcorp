<?php 
error_reporting(0);
Class dir{
    const Sort = "Sort";
    protected static $_instance;

    private function __construct(){}

    private function __clone(){}

    public static function getInstance(){
        if (null === self::$_instance){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getdir(){
        $dir = '/wamp/www/tzCorp';
        $file = scandir($dir);
        unset($file[0]);
        unset($file[1]);
        return $file;
    }

    public function SortSizeAsc(){
        $file = dir::getInstance()->getdir();
        $filesize = array();
        // var_dump($file);
        foreach ($file as $size) {
            $file_size = stat($size);
            $filesize[$size] = $file_size['size'];
        }
        arsort($filesize);
        echo "<h3>Файлы директории</h3>";
        foreach ($filesize as $key => $value) {
            if($value == 0){
                echo $key ." - это директория"."<br><br>";  
            }else{
            echo $key . "<br><br>";
            }
        }
    }

    public function SortSizeDesc(){
        $file = dir::getInstance()->getdir();
        $filesize = array();
        foreach ($file as $size) {
            $file_size = stat($size);
            $filesize[$size] = $file_size['size'];
        }
        asort($filesize);
        echo "<h3>Файлы директории</h3>";
        foreach ($filesize as $key => $value) {
            if($value == 0){
                echo $key ." - это директория"."<br><br>";  
            }else{
            echo $key . "<br><br>";
            }
        }
    }

    public function SortNameAsc(){
        $file = dir::getInstance()->getdir();
        asort($file);       
        echo "<h3>Файлы директории</h3>";
        foreach ($file as $value) {
            if(is_dir($value)){
                echo $value ." - это директория";  
            }else{
            echo $value . "<br><br>";
            }
        }

    }

    public function SortNameDesc(){
        $file = dir::getInstance()->getdir();
        arsort($file);
        echo "<h3>Файлы директории</h3>";
        foreach ($file as $value) {
            if(is_dir($value)){
                echo $value ." - это директория";  
            }else{
            echo $value . "<br><br>";
            }
        }
    }

    public function SortPhp(){
        $file = dir::getInstance()->getdir();
        echo "<h3>Файлы директории</h3>";
        foreach ($file as $name) {
            $file_name = pathinfo($name);
            if($file_name['extension'] == 'php'){
                echo $name."<br><br>";
            }
        }
        // var_dump($file);
    }

    public function SortTxt(){
        $file = dir::getInstance()->getdir();
        echo "<h3>Файлы директории</h3>";
        foreach ($file as $name) {
            $file_name = pathinfo($name);
            if($file_name['extension'] == 'txt'){
                echo $name."<br><br>";
            }
        }
    }
}
if(isset($_POST['sort'])){

    $selectOrder = $_POST['sort'];
    if($selectOrder == 0){
        dir::getInstance()->SortSizeAsc();
    }
    elseif($selectOrder == 1){
        dir::getInstance()->SortSizeDesc();
    }
    elseif($selectOrder == 2){
        dir::getInstance()->SortNameAsc();
    }
    elseif($selectOrder == 3){
        dir::getInstance()->SortNameDesc();
    }
    elseif($selectOrder == 4){
        dir::getInstance()->SortPhp();
    }
    elseif($selectOrder == 5){
        dir::getInstance()->SortTxt();
    }
}
else{
    echo "<h3>Файлы директории</h3>";
    foreach (dir::getInstance()->getdir() as $value) {
        if(is_dir($value)){
            echo $value ." - это директория";  
        }else{
        echo $value . "<br><br>";
        }
        // var_dump($value);
    }
}