<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script type="text/javascript" src="filter.js"></script>
    </head>
    <body>
        <form action="file.php" method="post">
            <p>Сотировка</p>
            <select id="filter" onchange="typeOfPage();">
                <option>выбрать</option>
                <option value="0">размер по убыванию</option>
                <option value="1">размер по возрастанию</option>
                <option value="2">от А до Я</option>
                <option value="3">от Я до А</option>
                <option value="4">только PHP файлы</option>
                <option value="5">только TXT файлы</option>
            </select>
        </form>
        <div id="filter_all">
            <?php
            include "file.php";
            ?>
        </div>
    </body>
</html>
<script type="text/javascript">
function typeOfPage(){
    var sort = $('#filter').val();
        $.ajax({
            type: "POST",
            url: "file.php",
            data: {sort: sort},
            success: function(data) {
                $('#filter_all').html(data);
            }
        });
};
</script>